extern crate ghakuf;
extern crate rand;
use ghakuf::messages::*;
use ghakuf::reader::*;
use ghakuf::writer::*;
use rand::prelude::*;

use std::path;

mod analytics;

fn main() {
    let mut rng = thread_rng();
    let path = path::Path::new("/home/ro6aff/Compmus/examples/elise.mid");
    let mut messages: Vec<Message> = Vec::new();
    {
        let mut handler = HogeHandler {
            messages: &mut messages,
        };
        let mut reader = Reader::new(&mut handler, &path).unwrap();
        let _ = reader.read();
    }
    let mut notes: Vec<(u8, u32)> = vec![];
    for i in messages {
        match i.clone() {
            Message::MidiEvent {
                delta_time: dt,
                event: e,
            } => match e {
                MidiEvent::NoteOn {
                    ch: _,
                    note: n,
                    velocity: _,
                } => {
                    notes.push((n, dt));
                }
                _ => {}
            },
            _ => {}
        }
    }
    let tempo: u32 = 10;
    let mut write_messages: Vec<Message> = vec![
        Message::MetaEvent {
            delta_time: 0,
            event: MetaEvent::SetTempo,
            data: [(tempo >> 16) as u8, (tempo >> 8) as u8, tempo as u8].to_vec(),
        },
        Message::MetaEvent {
            delta_time: 0,
            event: MetaEvent::EndOfTrack,
            data: Vec::new(),
        },
    ];
        let plok = analytics::construct_probalility_table(notes);
        write_messages.push(Message::TrackChange);
        let mut note = (60, 8);
        for i in plok.keys() {
            note = *i;
            if rng.gen::<f32>() >= 0.5 {
                break;
            }
        }
        for _ in 0..1000 {
            match note {
                (a, b) => {
                    write_messages.push(Message::MidiEvent {
                        delta_time: 0,
                        event: MidiEvent::NoteOn {
                            ch: 0,
                            note: a,
                            velocity: 127,
                        },
                    });
                    write_messages.push(Message::MidiEvent {
                        delta_time: b,
                        event: MidiEvent::NoteOff {
                            ch: 0,
                            note: a,
                            velocity: 127,
                        },
                    });
                }
            }
            let mut blqh: f32 = rng.gen();
            let mut currc = 0.0;
            for j in &plok[&note] {
                match j {
                    (a, b) => {
                        currc += b;
                        if blqh <= currc {
                            note = *a;
                            break;
                        }
                    }
                }
            }
        }
        write_messages.push(Message::MetaEvent {
            delta_time: 0,
            event: MetaEvent::EndOfTrack,
            data: Vec::new(),
        });
    let path = path::Path::new("/tmp/result_no_tracks_no_channels.mid");
    let mut writer = Writer::new();
    writer.running_status(true);
    for message in &write_messages {
        writer.push(&message);
    }
    let _ = writer.write(&path);
}

struct HogeHandler<'a> {
    messages: &'a mut Vec<Message>,
}
impl<'a> Handler for HogeHandler<'a> {
    fn meta_event(&mut self, delta_time: u32, event: &MetaEvent, data: &Vec<u8>) {
        self.messages.push(Message::MetaEvent {
            delta_time: delta_time,
            event: event.clone(),
            data: data.clone(),
        });
    }
    fn midi_event(&mut self, delta_time: u32, event: &MidiEvent) {
        self.messages.push(Message::MidiEvent {
            delta_time: delta_time,
            event: event.clone(),
        });
    }
    fn sys_ex_event(&mut self, delta_time: u32, event: &SysExEvent, data: &Vec<u8>) {
        self.messages.push(Message::SysExEvent {
            delta_time: delta_time,
            event: event.clone(),
            data: data.clone(),
        });
    }
    fn track_change(&mut self) {
        if self.messages.len() > 0 {
            self.messages.push(Message::TrackChange)
        }
    }
}
