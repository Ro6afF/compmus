import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import rfft, irfft
from scipy.io import wavfile
import json
import math
from random import random

def arr2str(arr):
    ans = ''
    for i in np.array(arr).ravel():
        ans += str(i) + ' '
    return ans

def str2arr(s):
    q = s.split()
    return np.array([int(i) for i in q])

fs, data1 = wavfile.read('/home/ro6aff/Compmus/wav/asdf/test.wav')
resol = 2
blq = {}
bla = {}
data = [np.array([0, 0], dtype=np.int16) for i in range(resol+1)] + [i for i in data1] + [np.array([0, 0], dtype=np.int16) for i in range(resol+1)]

for i in range(0, len(data) - resol, resol):
    a = arr2str(np.array(data[i:i+resol]))
    b = arr2str(data[i+resol:i+resol+resol])
    # a = np.array2string(rfft(data[i:i+resol]))
    # b = np.array2string(rfft(data[i+resol:i+2*resol]))
    try:
        bla[a] += 1
    except:
        bla[a] = 1

    try:
        blq[a][b] += 1
    except:
        try:
            blq[a][b] = 0
        except:
            blq[a] = {}
            blq[a][b] = 0
        blq[a][b] += 1
prob = {}
for k in blq:
    for p in blq[k]:
        try:
            prob[k][p] = blq[k][p] / bla[k]
        except:
            prob[k] = {}
            prob[k][p] = blq[k][p] / bla[k]

chos = data[0:resol]
ans = []
for _ in range(len(data)//resol):
    for i in chos:
        ans.append(i)
    q = arr2str(np.array(chos))
    chans = random()
    curr = 0.0
    if q not in prob:
        break
    try:
        for i in prob[q]:
            curr += prob[q][i]
            if chans <= curr:
                chos = str2arr(i).reshape(np.array(chos).shape)
                break
    except:
        break
wavfile.write('out4.wav', fs, np.array(ans, dtype=np.int16))
