import midi
import numpy
import sys
import json


def midiToNoteStateMatrix(midifile, compact=False):
    lowerBound = 0
    upperBound = 128
    lowerBoundInst = 0
    upperBoundInst = 128
    
    pattern = midi.read_midifile(midifile)

    if compact:
        lowerBound = 200
        upperBound = -1
        lowerBoundInst = 200
        upperBoundInst = -1
        for i in pattern:
            for j in i:
                if isinstance(j, midi.NoteOnEvent):
                    lowerBound = min(lowerBound, j.pitch)
                    upperBound = max(upperBound, j.pitch)
                if isinstance(j, midi.ProgramChangeEvent):
                    lowerBoundInst = min(j.data[0], lowerBoundInst)
                    upperBoundInst = max(j.data[0], upperBoundInst)
        bounds = {}
        try:
            boundFile = open('./bounds', 'r')
            bounds = json.loads(boundFile.read())
            boundFile.close()
        except:
            pass
        boundFile = open('./bounds', 'w')
        bounds[midifile] = [lowerBound, upperBound, lowerBoundInst, upperBoundInst]
        boundFile.write(json.dumps(bounds))
        boundFile.close()

    timeleft = [track[0].tick for track in pattern]

    posns = [0 for track in pattern]

    statematrix = []
    span = upperBound - lowerBound + 1
    spanInst = upperBoundInst - lowerBoundInst + 1
    time = 0

    state = [[[0, 0] for x in range(span)] for y in range(spanInst)]
    statematrix.append(state)
    inst = {}
    while True:
        if time % (pattern.resolution / 4) == (pattern.resolution / 8):
            # Crossed a note boundary. Create a new state, defaulting to holding notes
            oldstate = state
            state = [[[oldstate[y][x][0], 0]
                      for x in range(span)] for y in range(spanInst)]
            statematrix.append(state)

        for i in range(len(timeleft)):
            while timeleft[i] == 0:
                track = pattern[i]
                pos = posns[i]

                evt = track[pos]
                if isinstance(evt, midi.ProgramChangeEvent):
                    if i not in inst:
                        inst[i] = {}
                    inst[i][evt.channel] = evt.data[0]
                if isinstance(evt, midi.NoteEvent):
                    if i not in inst:
                        inst[i] = {}
                    if evt.channel not in inst[i]:
                        inst[i][evt.channel] = 0
                    if isinstance(evt, midi.NoteOffEvent) or evt.velocity == 0:
                        state[inst[i][evt.channel]-lowerBoundInst][evt.pitch -
                                                                    lowerBound] = [0, 0]
                    else:
                        state[inst[i][evt.channel]-lowerBoundInst][evt.pitch -
                                                                    lowerBound] = [evt.velocity, 1]
                try:
                    timeleft[i] = track[pos + 1].tick
                    posns[i] += 1
                except IndexError:
                    timeleft[i] = None

            if timeleft[i] is not None:
                timeleft[i] -= 1
        
        if all(t is None for t in timeleft):
            break

        time += 1

    statematrix += statematrix[:1]
    return statematrix


def noteStateMatrixToMidi(statematrix, name, nameorig=None):
    if nameorig is not None:
        bounds = {}
        boundFile = open('./bounds', 'r')
        bounds = json.loads(boundFile.read())
        boundFile.close()
        [lowerBound, upperBound, lowerBoundInst, upperBoundInst] = bounds[nameorig]
    else:
        [lowerBound, upperBound, lowerBoundInst, upperBoundInst] = [0, 128, 0, 128]
    statematrix = numpy.asarray(statematrix)
    pattern = midi.Pattern()

    span = upperBound-lowerBound+1
    spanInst = upperBoundInst - lowerBoundInst + 1
    tickscale = 55

    tracks = [midi.Track() for x in range(spanInst)]
    for i in tracks:
        pattern.append(i)
        i.append(midi.ProgramChangeEvent(data=[lowerBoundInst]))
        lowerBoundInst += 1

    prevstate = [[[0, 0] for x in range(span)] for y in range(spanInst)]
    lastcmdtime = [0 for x in range(spanInst)]
    for time, stateG in enumerate(statematrix + [prevstate[:]]):
        for inst, state in enumerate(stateG):
            offNotes = []
            onNotes = []
            for i in range(span):
                n = state[i]
                p = prevstate[inst][i]
                if p[0] != 0:
                    if n[0] == 0:
                        offNotes.append(i)
                    elif n[1] != 0:
                        offNotes.append(i)
                        onNotes.append([i, n[0]])
                elif n[0] != 0:
                    onNotes.append([i, n[0]])
            for note in offNotes:
                tracks[inst].append(midi.NoteOffEvent(tick=(time-lastcmdtime[inst])
                                            * tickscale, pitch=note+lowerBound))
                lastcmdtime[inst] = time
            for note in onNotes:
                tracks[inst].append(midi.NoteOnEvent(tick=(time-lastcmdtime[inst]) *
                                            tickscale, velocity=note[1], pitch=note[0]+lowerBound))
                lastcmdtime[inst] = time

        prevstate = stateG

    for i in tracks:
        i.append(midi.EndOfTrackEvent(tick=1))
    midi.write_midifile("{}".format(name), pattern)

if __name__ == "__main__":
    if sys.argv[1] == 'read':
        print(json.dumps(midiToNoteStateMatrix(sys.argv[2], True)))
    else:
        noteStateMatrixToMidi(json.loads(input()), sys.argv[2], sys.argv[3])
