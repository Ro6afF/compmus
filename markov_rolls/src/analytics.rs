use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::thread;

pub fn construct_probalility_table(
    a: Vec<Vec<Vec<Vec<u8>>>>,
) -> HashMap<Vec<Vec<Vec<u8>>>, Vec<(Vec<Vec<Vec<u8>>>, f32)>> {
    let mut next: HashMap<Vec<Vec<Vec<u8>>>, Vec<Vec<Vec<Vec<u8>>>>> = HashMap::new();
    if a.len() == 0 {
        return HashMap::new();
    }
    for i in 0..(a.len() - 1) {
        (*next.entry(a[i].clone()).or_insert(vec![])).push(a[i + 1].clone());
    }
    let ans: Arc<Mutex<HashMap<Vec<Vec<Vec<u8>>>, Vec<(Vec<Vec<Vec<u8>>>, f32)>>>> =
        Arc::new(Mutex::new(HashMap::new()));
    let mut threads = vec![];
    for (k, v) in next {
        let ans = ans.clone();
        threads.push(thread::spawn(move || {
            let mut map: HashMap<Vec<Vec<Vec<u8>>>, u32> = HashMap::new();
            let mut cnt: f32 = 1.0;
            for q in v {
                *map.entry(q).or_insert(0) += 1;
                cnt += 1.0;
            }
            let mut res = vec![];
            for (k1, v1) in map {
                res.push((k1, v1 as f32 / cnt));
            }
            let mut ans = ans.lock().unwrap();
            (*ans).insert(k, res);
        }));
    }
    for i in threads {
        i.join().unwrap();
    }
    let ans = ans.lock().unwrap();
    ans.clone()
}
