mod analytics;
mod midi_roll;
extern crate rand;
use rand::prelude::*;
use std::env;

fn main() {
    let mut rng = thread_rng();
    let mut rolls = vec![];
    let orig = env::args().nth(1).unwrap();
	println!("{:?}", orig);
    for j in midi_roll::get_roll(&orig) {
        rolls.push(j)
    }
    let plok = analytics::construct_probalility_table(rolls);
    for smlqk in 0..10 {
        let mut result: Vec<Vec<Vec<Vec<u8>>>> = vec![];
        let mut curr_t = None;
        for (k, _) in &plok {
            curr_t = Some(k);
            break;
        }
        let mut curr = match curr_t {
            Some(x) => x,
            _ => panic!(),
        };
        for _ in 0..1000 {
            result.push(curr.clone());
            let blqh: f32 = rng.gen();
            let mut currc = 0.0;
            for j in &plok[&curr.clone()] {
                match j {
                    (a, b) => {
                        currc += b;
                        if blqh <= currc {
                            curr = &a;
                            break;
                        }
                    }
                }
            }
        }
        midi_roll::write_roll(result, &format!("/tmp/plok{}.mid", smlqk), &orig);
    }
}
