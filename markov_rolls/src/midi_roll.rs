use std::io::Write;
use std::process::{Command, Stdio};

extern crate serde_json;

pub fn get_roll(file: &str) -> Vec<Vec<Vec<Vec<u8>>>> {
    let output = Command::new("python")
        .args(&[
            "/home/ro6aff/Compmus/markov_rolls/midi_to_statematrix.py",
            "read",
            file,
        ])
        .output()
        .expect("failed to execute process");
    let json_roll: Vec<Vec<Vec<Vec<u8>>>> = serde_json::from_slice(&output.stdout).unwrap();
    json_roll
}

pub fn write_roll(roll: Vec<Vec<Vec<Vec<u8>>>>, file: &str, orig: &str) {
    let mut child = Command::new("python")
        .args(&[
            "/home/ro6aff/Compmus/markov_rolls/midi_to_statematrix.py",
            "write",
            file,
			orig,
        ])
        .stdin(Stdio::piped())
        .spawn()
        .expect("failed to execute process");
    {
        let stdin = child.stdin.as_mut().expect("Failed to open stdin");
        stdin
            .write_all(serde_json::to_string(&roll).unwrap().as_bytes())
            .expect("Failed to write to stdin");
    }
    let _ = child.wait();
}
