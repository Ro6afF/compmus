import midi
import numpy as np
import sys
import json


def read_midi(path_to_file):
    pattern = midi.read_midifile(path_to_file)
    step = pattern.resolution // 16
    velocities = None
    replays = None

    for track in pattern:
        track_velocities = []
        track_replays = []

        velocity_buffer = [np.zeros((128))]
        replay_buffer = [np.zeros((128))]
        for _, event in enumerate(track):
            for _ in range(event.tick):
                replay_buffer.append(np.zeros(128))
                velocity_buffer.append(np.copy(velocity_buffer[-1]))

                if len(velocity_buffer) > step:
                    track_replays.append(np.minimum(
                        np.sum(replay_buffer[:-1], axis=0), 1))
                    track_velocities.append(
                        np.amax(velocity_buffer[:-1], axis=0))
                    replay_buffer = replay_buffer[-1:]
                    velocity_buffer = velocity_buffer[-1:]

            if isinstance(event, midi.NoteOnEvent):
                pitch, velocity = event.data
                velocity_buffer[-1][pitch] = velocity

                if len(velocity_buffer) > 1 and velocity_buffer[-2][pitch] > 0 and velocity_buffer[-1][pitch] > 0:
                    replay_buffer[-1][pitch] = 1
                    velocity_buffer[-1][pitch] = velocity_buffer[-2][pitch]

            if isinstance(event, midi.NoteOffEvent):
                pitch, velocity = event.data
                velocity_buffer[-1][pitch] = 0

            if isinstance(event, midi.EndOfTrackEvent):
                break
        track_replays.append(np.minimum(
            np.sum(replay_buffer, axis=0), 1))
        track_velocities.append(velocity_buffer[0])
        if velocities is None:
            velocities = track_velocities
            replays = track_replays
        else:
            if len(track_velocities) > len(velocities):
                tmp = track_velocities
                track_velocities = velocities
                velocities = tmp

                tmp = track_replays
                track_replays = replays
                replays = tmp

            diff = len(velocities) - len(track_velocities)
            velocities += np.pad(track_velocities,
                                 ((0, diff), (0, 0)), 'constant')
            replays += np.pad(track_replays,
                              ((0, diff), (0, 0)), 'constant')
    return np.minimum(np.stack([velocities, replays], axis=2), 1)


def write_midi(notes, path_to_file):
    pattern = midi.Pattern()
    pattern.resolution = 16
    track = midi.Track()
    pattern.append(track)

    velocity = notes[:, :, 0]
    replay = notes[:, :, 1]

    current = np.zeros_like(velocity[0])
    last_event_tick = 0

    for tick, data in enumerate(velocity):
        data = np.array(data)

        if not np.array_equal(current, data):
            for index, next_velocity in np.ndenumerate(data):
                if next_velocity > 0 and current[index] == 0:
                    track.append(midi.NoteOnEvent(tick=tick - last_event_tick,
                                                  velocity=int(
                                                      velocity[tick][index[0]]),
                                                  pitch=index[0]))
                    last_event_tick = tick
                elif current[index] > 0 and next_velocity == 0:
                    track.append(midi.NoteOffEvent(
                        tick=(tick - last_event_tick), pitch=index[0]))
                    last_event_tick = tick
                elif current[index] > 0 and next_velocity > 0 and replay[tick][index[0]] > 0:
                    track.append(midi.NoteOffEvent(
                        tick=(tick - last_event_tick), pitch=index[0]))
                    track.append(midi.NoteOnEvent(
                        tick=0,
                        velocity=int(velocity[tick][index[0]]),
                        pitch=index[0]))
                    last_event_tick = tick

        current = data

    tick += 1

    for index, vel in np.ndenumerate(current):
        if vel > 0:
            track.append(midi.NoteOffEvent(tick=tick - last_event_tick,
                                           pitch=index[0]))
            last_event_tick = tick
    track.append(midi.EndOfTrackEvent(tick=1))
    midi.write_midifile(path_to_file, pattern)

if __name__ == "__main__":
    if sys.argv[1] == 'read':
        print(json.dumps(read_midi(sys.argv[2]).tolist()))
    elif sys.argv[1] == 'write':
        write_midi(json.loads(input()), sys.argv[2])
    else:
        print("read or write")

#(json.dumps(read_midi('./test.mid').tolist()))
