import tensorflow as tf
from constants import OCTAVE_UNITS, OCTAVE, INPUT_DROPOUT_RATE, DROPOUT_RATE, TIME_AXIS_LAYERS, MIDI_NOTES, TIME_AXIS_UNITS, NOTE_AXIS_LAYERS, NOTE_AXIS_UNITS, NOTE_UNITS, MODEL_FILE
from keras.layers import TimeDistributed, Conv1D, Activation, Dropout, Lambda, Permute, LSTM, Input, Reshape, Dense
from keras.models import Model
from keras.layers.merge import Concatenate


def pitch_pos_in_f(time_steps):
    def f(x):
        note_ranges = tf.range(MIDI_NOTES, dtype='float32') / MIDI_NOTES
        repeated_ranges = tf.tile(note_ranges, [tf.shape(x)[0] * time_steps])
        return tf.reshape(repeated_ranges, [tf.shape(x)[0], time_steps, MIDI_NOTES, 1])
    return f


def time_axis(notes):
    time_steps = int(notes.get_shape()[1])
    note_octave = TimeDistributed(Conv1D(OCTAVE_UNITS, 2 * OCTAVE, padding='same'))(notes)
    note_octave = Activation('tanh')(note_octave)
    note_octave = Dropout(DROPOUT_RATE)(note_octave)

    note_features = Concatenate()([
        Lambda(pitch_pos_in_f(time_steps))(notes),
        note_octave,
    ])

    note_features = Permute((2, 1, 3))(note_features)

    for _ in range(TIME_AXIS_LAYERS):
        note_features = LSTM(
            TIME_AXIS_UNITS, return_sequences=True)(note_features)
        note_features = Dropout(DROPOUT_RATE)(note_features)

    return note_features


def note_axis():
    lstm_layer_cache = {}
    note_dense = Dense(2, activation='sigmoid', name='note_dense')
    volume_dense = Dense(1, name='volume_dense')
    def f(x, chosen):
        time_steps = int(x.get_shape()[1])
        shift_chosen = Lambda(lambda x: tf.pad(x[:, :, :-1, :], [[0, 0], [0, 0], [1, 0], [0, 0]]))(chosen)
        shift_chosen = Reshape((time_steps, MIDI_NOTES, -1))(shift_chosen)
        x = Concatenate(axis=3)([x, shift_chosen])
        for l in range(NOTE_AXIS_LAYERS):
            if l not in lstm_layer_cache:
                lstm_layer_cache[l] = LSTM(NOTE_AXIS_UNITS, return_sequences=True)
            x = TimeDistributed(lstm_layer_cache[l])(x)
            x = Dropout(DROPOUT_RATE)(x)
        return Concatenate()([note_dense(x), volume_dense(x)])
    return f


def build_models():
    notes_in = Input((64, MIDI_NOTES, NOTE_UNITS))
    chosen_in = Input((64, MIDI_NOTES, NOTE_UNITS))

    notes = Dropout(INPUT_DROPOUT_RATE)(notes_in)
    chosen = Dropout(INPUT_DROPOUT_RATE)(chosen_in)

    time_out = time_axis(notes)
    notes_out = note_axis()(time_out, chosen)

    model = Model([notes_in, chosen_in], [notes_out])
    model.compile(optimizer='nadam')
    from keras.utils import plot_model
    plot_model(model, to_file='model.png')

    return [model]

def build_or_load(allow_load=True):
    models = build_models()
    models[0].summary()
    if allow_load:
        try:
            models[0].load_weights(MODEL_FILE)
            print('Loaded model from file.')
        except:
            print('Unable to load model from file.')
    return models
