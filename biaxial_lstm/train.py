from keras.callbacks import ModelCheckpoint, EarlyStopping, TensorBoard
from constants import MODEL_FILE, BATCH_SIZE, SEQUENCE_LENGTH
import os
from midi_connecion import read_midi
from model import build_or_load
import numpy as np

def load_all():
    sequences = []
    for i, _, k in os.walk('./data/beeth'):
        for l in k:
            sequences.append(read_midi(os.path.join(i, l)))

    dataX = []
    dataY = []
    for seq in sequences:
        for i in range(len(seq) - 1):
            dataX.append(seq[i:i+1])
            dataY.append(seq[i+1:i+2])
    return [np.array(dataX), np.array(dataY)], np.array(dataY)


def train():
    print('Loading data')
    train_input, train_output = load_all()

    cbs = [
        ModelCheckpoint(MODEL_FILE, monitor='loss', save_best_only=True, save_weights_only=True),
        EarlyStopping(monitor='loss', patience=5),      
        TensorBoard(log_dir='./logs', histogram_freq=1)
    ]
    print('Training...')
    models = build_or_load()
    models[0].fit(train_input, train_output, epochs=1000, callbacks=cbs, batch_size=BATCH_SIZE)

train()